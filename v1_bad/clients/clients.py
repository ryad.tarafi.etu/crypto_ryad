import os
def client(client_id,client_name,price):
    '''
    Le client génére un chèque de la forme : 
    [Data|cle publique client|flag]
    Puis hashage de ce chèque et chiffrement de ce chèque avec la clé privé de la banque
    Puis, on chiffre le hash avec la clé privée du client et on concatène le résultat avec le reste du chèque en insérant au milieu un flag
    pour nous permettre de split dans le fichier commerces.py

    Data = id_client,id_commercant,nom,valeur du chèque
    '''
    print("Génération du chèque ...")
    try:
        file = open(client_name+"_"+client_id+"/"+client_name+"_"+client_id+".public")
        cle_pub = file.read()


        data = client_id+";"+client_name+";"+price+";"+cle_pub
        file_temp = open(client_name+"_"+client_id+"/"+client_name+"_"+client_id+".temp","w+") #w+ crée le fichier si existe pas
        file_temp.write(data)
        file_temp.close()
        os.system("sha256sum "+client_name+"_"+client_id+"/"+client_name+"_"+client_id+".temp | awk '{print $1}' > "+client_name+"_"+client_id+"/hash.temp")
        os.system("openssl rsautl -in "+client_name+"_"+client_id+"/"+client_name+"_"+client_id+".temp -out "+client_name+"_"+client_id+"/cheque.encrypt -inkey ../banque/banque.private -encrypt")
        os.system("openssl rsautl -in "+client_name+"_"+client_id+"/hash.temp -out "+client_name+"_"+client_id+"/hash.encrypt_by_client_private -inkey "+client_name+"_"+client_id+"/"+client_name+"_"+client_id+".private -encrypt")

        os.system("zip -r -j "+client_name+"_"+client_id+"/cheque.zip " + client_name+"_"+client_id+"/cheque.encrypt " + client_name+"_"+client_id+"/hash.temp")
        
    
        os.system("mv "+client_name+"_"+client_id+"/cheque.zip ../commerces/"+client_name+"_"+client_id+"/cheque.zip")

        #os.system("rm "+client_name+"_"+client_id+"/hash.temp")
    except:
        print("Le client n'existe pas. \n Abandon... ")
    print("Chèque généré et envoyé au commerce")
    return True

def verify_facture(client_id,client_name,cheque_value):
    '''
    Vérifie la facture émise par le commerce avant d'envoyer le chèque
    '''
    try:
        os.system("unzip "+client_name+"_"+client_id+"/facture.zip -d "+client_name+"_"+client_id+"/")
        file = open(client_name+"_"+client_id+"/"+client_name+"_"+client_id+".facture")
        facture = file.read()
        facture_client_id = facture.split(";")[1]
        price = facture.split(";")[3]
        file.close()
        if (client_id != facture_client_id) or (cheque_value != price):
            print("La facture n'est pas correcte. \n Abandon... ")
            exit()
        os.system("sha256sum "+client_name+"_"+client_id+"/"+client_name+"_"+client_id+".facture | awk '{print $1}' > "+client_name+"_"+client_id+"/hash_diff.temp")

        
        if os.system("diff "+client_name+"_"+client_id+"/hash.temp "+client_name+"_"+client_id+"/hash_diff.temp") != 0:
            print("Hash dfférent, facture corrompu. \n Abandon... ")
            exit()
    except:
        print("Le client n'existe pas. \n Abandon... ")

    os.system("rm "+client_name+"_"+client_id+"/hash.temp")
    os.system("rm "+client_name+"_"+client_id+"/hash_diff.temp")
    return True


if __name__ == "__main__":
    client_id = input("Entrez l'id du client : ")
    client_name = input("Entrez le nom du client : ")
    cheque_value = input("Entrez la valeur du cheque : ")
    if (verify_facture(client_id,client_name,cheque_value)):
        client(client_id,client_name,cheque_value)






