import os
import random
from zipfile import ZipFile

def commerces(client_id,client_name,price):
    '''
    Déchiffre la première partie du chèque avec la clé publique de la banque
    Avec la clé publique du client trouvé dans le chèque, on déchiffre la seconde partie à savoir le hash.
    Ensuite, on hash la première partie du chèque et on compare avec le résultat envoyé dans le chèque.


    3 possibilités d'échecs : 

    -   Mauvais déchiffrement avec la clé publique de la banque
    -   Mauvais déchiffremement avec la clé publique du client
    -   Valeur du hash différent
    '''

    
    file = open("./commerces.registre","a+")
    facture_id = str(random.randint(1,50000))
    facture = client_name + ";" + client_id + ";" + facture_id + ";" + price
    file.write(facture + '\n')
    file.close()
    try:
        file_temp = open(client_name+"_"+client_id+"/"+client_name+"_"+client_id+".facture","w+") #w+ crée le fichier si existe pas
        file_temp.write(facture)
        file_temp.close()
        os.system("sha256sum "+client_name+"_"+client_id+"/"+client_name+"_"+client_id+".facture | awk '{print $1}' > "+client_name+"_"+client_id+"/hash.temp")
        os.system("zip -r -j "+client_name+"_"+client_id+"/facture.zip "+ client_name+"_"+client_id+"/"+client_name+"_"+client_id+".facture " + client_name+"_"+client_id+"/hash.temp")
        os.system("cp "+client_name+"_"+client_id+"/facture.zip ../clients/"+client_name+"_"+client_id+"/facture.zip")
        os.system("rm "+client_name+"_"+client_id+"/hash.temp")
        os.system("rm "+client_name+"_"+client_id+"/"+client_name+"_"+client_id+".facture")
    except:
        print("Le client n'existe pas. \n Abandon... ")


if __name__ == "__main__":
    client_id = input("Entrez l'id du client : ")
    client_name = input("Entrez le nom du client : ")
    price = input("Entrez le prix : ")
    commerces(client_id,client_name,price)