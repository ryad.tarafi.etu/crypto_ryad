import os
import random

def banque(client_id,client_name):
    '''
    La banque génére une clé publique et privée pour chaque client
    La banque génére une clé publique et privée pour elle
    Elle "envoie" la paire de clé du client au client
    Elle "envoie" sa clé privée au client
    Elle "envoie" sa clé publique au commercant"
    '''

    os.system("mkdir ../clients/"+ client_name+"_"+client_id)
    os.system("mkdir ../commerces/"+ client_name+"_"+client_id)
    os.system("openssl genrsa -out " + client_name+"_"+client_id+".private 4096")
    os.system("openssl rsa -in " + client_name+"_"+client_id+".private -pubout -out "+client_name+"_"+client_id+".public")
    os.system("mv "+client_name+"_"+client_id+".* ../clients/"+client_name+"_"+client_id)


def banque_keys():
    '''
    Génére la pair publique/privée de la banque
    '''
    os.system("openssl genrsa -out banque.private 4096")
    os.system("openssl rsa -in banque.private -pubout -out banque.public")



if __name__ == "__main__":
    if not os.path.isfile('./banque.private') or not os.path.isfile('./banque.public'):
        banque_keys()
    client_name = input("Entrez le nom du client : ")
    client_id = str(random.randint(1,50000))
    file = open("./banque.registre","a+")
    while client_id in file.read():
        client_id = str(random.randint(1,50000))
    file.write(client_name + ";" + client_id + '\n')
    file.close()
    banque(client_id,client_name)