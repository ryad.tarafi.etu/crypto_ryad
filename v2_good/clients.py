import os

def sign(shop_id,client_id,facture_id):
    '''
    Sign check information with client private key
    '''

    os.system("openssl dgst -sha256 -sign ./clients/"+client_id+"/client_"+client_id+"_private.pem -out ./clients/"+client_id+"/"+client_id+"_cheque.sha256 ./clients/"+client_id+"/cheque_"+client_id+"_"+shop_id+"_"+facture_id+".cheque")
    #os.system("openssl base64 -in ./shops/"+shop_id+"/"+shop_id+".sha256 -out ./shops/"+shop_id+"/"+id+".factureB64")

def verify_facture_and_shop(shop_id,client_id,facture_id):
    '''
    '''

    if (os.system("openssl dgst -sha256 -verify ./bank_public.pem -signature ./clients/"+client_id+"/shop_"+shop_id+".sha256 ./clients/"+client_id+"/si_"+shop_id+".identity") != 0):
        return False # shop identity errone
    
    if (os.system("openssl dgst -sha256 -verify ./clients/"+client_id+"/shop_"+shop_id+"_public.pem -signature ./clients/"+client_id+"/"+shop_id+"_facture.sha256 ./clients/"+client_id+"/facture_"+client_id+"_"+facture_id) != 0):
        return False # facture errone
    
    return True



def create_check(shop_id,client_id,facture_id):
    '''
    '''
    temp = open("./clients/"+client_id+"/facture_"+client_id+"_"+facture_id,"r")
    price = temp.read().split(";")[3]
    temp.close()
    data = client_id+";"+shop_id+";"+facture_id+";"+price

    cheque = open("./clients/"+client_id+"/cheque_"+client_id+"_"+shop_id+"_"+facture_id+".cheque","w+")
    cheque.write(data)
    cheque.close()

    sign(shop_id,client_id,facture_id)

def send_check(shop_id,client_id,facture_id):
    '''
    '''
    os.system("cp ./clients/"+client_id+"/cheque_"+client_id+"_"+shop_id+"_"+facture_id+".cheque ./shops/"+shop_id+"/") #le cheque
    os.system("cp ./clients/"+client_id+"/"+client_id+"_cheque.sha256 "+"./shops/"+shop_id+"/") # signé du cheque
    os.system("cp ./clients/"+client_id+"/client_"+client_id+"_public.pem "+"./shops/"+shop_id+"/") # clé pub client
    os.system("cp ./clients/"+client_id+"/ic_"+client_id+".identity "+ "./shops/"+shop_id+"/") # carte identité
    os.system("mv ./clients/"+client_id+"/"+client_id+".sha256 ./clients/"+client_id+"/client_"+client_id+".sha256") # rename to avoid issue with client name
    os.system("cp ./clients/"+client_id+"/client_"+client_id+".sha256 ./shops/"+shop_id+"/") # signé de l'identité
