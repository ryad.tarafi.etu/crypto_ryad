# TP 1 crypto 

Ryad tarafi
Barlet Denis
Lebas Samuel


## Pré-requis

Il faut 2 dossiers avant d'éxécuter le main : 

    - clients
    - shops

## Déroulement 

### Phase 1

    - La banque génere sa paire de clé 
    - La banque crée un dossier pour un client
        - La banque génére une paire de clé pour ce client
        - La banque crée une identity card pour ce client (un id unique)
        - La banque signe cette carte d'identité avec sa clé privée (clé privée de la banque)
        - La banque donne sa clé publique à ce client et lui donne également son identity card
    - La banque crée un dossier pour un commerce
        - La banque génére une paire de clé pour ce commerce
        - La banque crée une identity card pour ce commerce (si) (un id unique)
        - La banque signe cette carte d'identité avec sa clé privée (clé privée de la banque)*
        - La banque donne sa clé publique à ce commerce et lui donne également sa carte d'identité

### Phase 2

    - Le commerce envoie une facture au client
    - Le commerce envoie le signé de la facture fait par le commerce, la carte d'identité du commerce , la carte d'identité du commerce signée ainsi que sa clée publique

    - Le client vérifie la carte d'identité du commerce grace au signé et la clée publique de la banque
    - Le client vérifie la facture grace au signé et la clée publique du commerce
    - Le client crée un chèque grâce à la facture et crée le signé avec sa clée privée
    - Le client envoi au commerce son chèque, le chèque signé, sa carte d'identitée, sa carte d'identitée signée et sa clée publique.
    - Le commerce vérifie la carte d'identitée grâce au signé et à la clée publique de la banque
    - Le commerce vérifie le chèque grâce au signé et la clée publique du client
    - Le commerce vérifie dans son registre si le chèque n'est pas un doublon (chèque déjà réalisé)

Si l'ensemble des éléments ci-dessus sont satisfaits, la transaction est correcte et est enregistré dans le registre du commerce 



