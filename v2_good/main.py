import bank
import clients
import shops


bank.bank_keys()
bank.create_client("1")
bank.create_shop("2")

shops.create_facture("1","1","2","100")
shops.send_facture("2","1","1")


if (clients.verify_facture_and_shop("2","1","1")):
    print("Commerce et facture vérifié")
    clients.create_check("2","1","1")
    clients.send_check("2","1","1")
    if (shops.verify_check_and_client("2","1","1")):
        print("Client et cheque vérifié")
        print("Transaction effectué avec succès")
    else:
        print("Erreur , cheque erroné ou doublon")
        print("Transaction abandonnée ...")
        exit()
else:
    print("Erreur commerce ou facture erroné")
    print("Abandon ...")
    