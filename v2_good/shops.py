import os

def sign(shop_id,client_id,facture_id):
    '''
    Sign shop information with bank private key
    '''

    os.system("openssl dgst -sha256 -sign ./shops/"+shop_id+"/shop_"+shop_id+"_private.pem -out ./shops/"+shop_id+"/"+shop_id+"_facture.sha256 ./shops/"+shop_id+"/facture_"+client_id+"_"+facture_id)
    #os.system("openssl base64 -in ./shops/"+shop_id+"/"+shop_id+".sha256 -out ./shops/"+shop_id+"/"+id+".factureB64")

def create_facture(facture_id, client_id, shop_id, price):
    data = client_id+";"+shop_id+";"+facture_id+";"+price
    facture = open("./shops/"+shop_id+"/facture_"+client_id+"_"+facture_id,"w+") #w+ crée le fichier si existe pas
    facture.write(data)
    facture.close()

    sign(shop_id,client_id,facture_id)


def send_facture(shop_id,client_id,facture_id):
    '''
    '''
    os.system("cp ./shops/"+shop_id+"/facture_"+client_id+"_"+facture_id+" ./clients/"+client_id+"/") #la facture
    os.system("cp ./shops/"+shop_id+"/"+shop_id+"_facture.sha256 "+"./clients/"+client_id+"/") # signé de la facture
    os.system("cp ./shops/"+shop_id+"/shop_"+shop_id+"_public.pem "+"./clients/"+client_id+"/") # clé pub shop
    os.system("cp ./shops/"+shop_id+"/si_"+shop_id+".identity "+ "./clients/"+client_id+"/") # shop identity
    os.system("mv ./shops/"+shop_id+"/"+shop_id+".sha256 ./shops/"+shop_id+"/shop_"+shop_id+".sha256") # rename to avoid issue with client name
    os.system("cp ./shops/"+shop_id+"/shop_"+shop_id+".sha256 ./clients/"+client_id+"/") # signé de l'identité



def verify_check_and_client(shop_id,client_id,facture_id):
    '''
    '''

    if (os.system("openssl dgst -sha256 -verify ./bank_public.pem -signature ./shops/"+shop_id+"/client_"+client_id+".sha256 ./shops/"+shop_id+"/ic_"+client_id+".identity") != 0):
        return False # client identity errone
    
    if (os.system("openssl dgst -sha256 -verify ./shops/"+shop_id+"/client_"+client_id+"_public.pem -signature ./shops/"+shop_id+"/"+client_id+"_cheque.sha256 ./shops/"+shop_id+"/cheque_"+client_id+"_"+shop_id+"_"+facture_id+".cheque") != 0):
        return False # cheque errone
    
    cheque = open("./shops/"+shop_id+"/cheque_"+client_id+"_"+shop_id+"_"+facture_id+".cheque","r")
    cheque_info = cheque.read()


    registry = open("./shops/"+shop_id+"/"+shop_id+".registry","a+") #a = append
    registry_info = registry.read()

    if cheque_info in registry_info:
        return False # check already in registry

    registry.write(cheque_info)
    registry.close()
    cheque.close()
    
    return True


def verify_registry():
    '''
    '''