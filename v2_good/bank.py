import os


def bank_keys():
    '''
    Create rsa key pair for the bank
    '''

    os.system("openssl genrsa -out ./bank_private.pem 4096")
    os.system("openssl rsa -in ./bank_private.pem -pubout -out ./bank_public.pem")


def sign(id,folder,kind):
    '''
    Sign client information with bank private key
    '''

    os.system("openssl dgst -sha256 -sign bank_private.pem -out ./"+folder+"/"+id+"/"+id+".sha256 ./"+folder+"/"+id+"/"+kind+"_"+id+".identity")
    #os.system("openssl base64 -in ./"+folder+"/"+id+"/"+id+".sha256 -out ./"+folder+"/"+id+"/"+id+".identityB64")

def create_client(client_id):
    '''
    '''
    os.system("mkdir clients/"+client_id+"/")
    os.system("cp ./bank_public.pem ./clients/"+client_id+"/") # Bank give public key to the client

    identity = open("./clients/"+client_id+"/ic_"+client_id+".identity","w+") # + = create file if don't exist
    identity.write(client_id)
    identity.close()

    sign(client_id,"clients","ic")

    os.system("openssl genrsa -out ./clients/"+client_id+"/client_"+client_id+"_private.pem 4096")
    os.system("openssl rsa -in ./clients/"+client_id+"/client_"+client_id+"_private.pem -pubout -out ./clients/"+client_id+"/client_"+client_id+"_public.pem")

def create_shop(shop_id):
    '''
    '''

    os.system("mkdir ./shops/"+shop_id)
    os.system("cp ./bank_public.pem ./shops/"+shop_id+"/")
    os.system ("touch ./shops/"+shop_id+"/"+shop_id+".registry")

    shop_identity = open("./shops/"+shop_id+"/si_"+shop_id+".identity","w+")
    shop_identity.write(shop_id)
    shop_identity.close()

    sign(shop_id,"shops","si")

    os.system("openssl genrsa -out ./shops/"+shop_id+"/shop_"+shop_id+"_private.pem 4096")
    os.system("openssl rsa -in ./shops/"+shop_id+"/shop_"+shop_id+"_private.pem -pubout -out ./shops/"+shop_id+"/shop_"+shop_id+"_public.pem")



